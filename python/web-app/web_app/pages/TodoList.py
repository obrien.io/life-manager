import os
from dataclasses import dataclass
from datetime import datetime
from typing import Dict, List, Optional

import dill
import streamlit as st
from streamlit_autorefresh import st_autorefresh

count = st_autorefresh(interval=10 * 1000)
now = datetime.now()
_TASKS_CACHE_PATH = f'{now.date().isoformat()}-tasks.dill'
_next_task_id = 1

st.write('Todo List!')


def on_change(*args, **kwargs):
    print("changed!", args, kwargs)


@dataclass
class Task:
    id: int
    name: str
    completed_at: Optional[datetime] = None

    @classmethod
    def create_task(cls, name: str):
        global _next_task_id
        task = Task(_next_task_id, name)
        _next_task_id += 1
        return task


def list_tasks_by_section(
    task_path: str = _TASKS_CACHE_PATH,
) -> Dict[str, List[Task]]:
    if not os.path.exists(task_path):
        return {
            "Morning routine": [
                Task.create_task("Out of bed"),
                Task.create_task("Make bed"),
                Task.create_task("Toilet"),
                Task.create_task("Weigh myself"),
                Task.create_task("Shower"),
                Task.create_task("Brush teeth"),
                Task.create_task("Deodorant"),
                Task.create_task("Shave (even days)"),
                Task.create_task("Get dressed"),
                Task.create_task("Brush hair"),
                Task.create_task("Coffee & Breakfast"),
                Task.create_task("Supplement"),
            ],
            "Before work": [
                Task.create_task("Air Conditioning off"),
                Task.create_task("Keys, wallet, mask, ID badge"),
                Task.create_task("Water bottle"),
                Task.create_task("Umbrella"),
                Task.create_task("Laptop & Charger, Phone charger"),
                Task.create_task("Airpods (both), Bose QC35 + cable"),
            ],
            "After work": [
                Task.create_task("Air Conditioning on"),
                Task.create_task("Exercise"),
                Task.create_task("Rinse off"),
                Task.create_task("Think about rest of the week"),
                Task.create_task("Eat dinner"),
            ],
            "Evening routine": [
                Task.create_task("Reset kitchen"),
                Task.create_task("Put away laundry"),
                Task.create_task("Pick out tomorrow's outfit"),
                Task.create_task("Take sleep aid"),
                Task.create_task("Brush teeth"),
                Task.create_task("Alarms set"),
                Task.create_task("Phone in another room"),
            ],
        }
    with open(task_path, 'rb') as f:
        return dill.load(f)


def _on_change(task: Task):
    task.completed_at = datetime.now() if not task.completed_at else None
    task_lists_by_section = list_tasks_by_section()
    for task_list in task_lists_by_section.values():
        for i, existing_task in enumerate(task_list):
            if existing_task.name == task.name:
                task_list[i] = task
                break
    with open(_TASKS_CACHE_PATH, 'wb') as f:
        dill.dump(task_lists_by_section, f)


def _add_task(task: Task, section: str):
    st.checkbox(
        task.name,
        on_change=lambda: _on_change(task),
        value=task.completed_at is not None,
        key=f"{section}-{task.name}",
    )


for section, _task_list in list_tasks_by_section().items():
    num_completed = len([x for x in _task_list if x.completed_at is not None])
    section_title = f"{section} - {num_completed}/{len(_task_list)}"
    with st.expander(section_title):
        for task in _task_list:
            _add_task(task, section)
