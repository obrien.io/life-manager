import requests
import streamlit as st
from streamlit_autorefresh import st_autorefresh

count = st_autorefresh(interval=10 * 1000)

st.write('Climate!')

CLIMATE_SERVER_HOST = "10.0.1.119"
CLIMATE_SERVER_PORT = 8000
r = requests.get(f"http://{CLIMATE_SERVER_HOST}:{CLIMATE_SERVER_PORT}/stats")
data = r.json()

co2, humidity, temp = data["co2"], data["humidity"], data["temp"]

col1, col2, col3 = st.columns(3)
col1.metric("Temperature", f"{round(temp, 1)} °F", None)
col2.metric("CO2", f"{int(co2)} PPM", None)
col3.metric("Humidity", f"{round(humidity, 1)}%", None)
