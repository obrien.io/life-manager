from collections import deque

from quart import Quart, request, jsonify

app = Quart(__name__)

MAX_LEN = 15
CO2_SCD40 = deque([], maxlen=MAX_LEN)
TEMP_SCD40 = deque([], maxlen=MAX_LEN)
HUMIDITY_SCD40 = deque([], maxlen=MAX_LEN)

CO2_SENSE_AIR = deque([], maxlen=MAX_LEN)
TEMP_SENSE_AIR = deque([], maxlen=MAX_LEN)
HUMIDITY_SENSE_AIR = deque([], maxlen=MAX_LEN)


def average(x: deque):
    return round(sum(list(x)) / len(list(x)), 2)


@app.route('/')
async def index():
    temp, co2, humidity = request.args.get("temp"), request.args.get("co2"), request.args.get(
        "humidity")
    temp, co2, humidity = (float(temp) * 1.8) + 32, float(co2), float(humidity)
    CO2_SCD40.appendleft(co2)
    TEMP_SCD40.appendleft(temp)
    HUMIDITY_SCD40.appendleft(humidity)
    return 'Ok'


@app.post('/sensors/<sensor_id>/measures')
async def sense_air_endpoint(sensor_id: str):
    post_data = await request.get_json()
    temp, co2, humidity = post_data.get("atmp"), post_data.get("rco2"), post_data.get("rhum")
    temp, co2, humidity = (float(temp) * 1.8) + 32, float(co2), float(humidity)
    CO2_SENSE_AIR.appendleft(co2)
    TEMP_SENSE_AIR.appendleft(temp)
    HUMIDITY_SENSE_AIR.appendleft(humidity)
    return 'Ok'


@app.route('/stats')
async def stats():
    return jsonify({
        "co2": (average(CO2_SCD40) + average(CO2_SENSE_AIR)) / 2,
        "temp": (average(TEMP_SCD40) + average(TEMP_SENSE_AIR)) / 2,
        "humidity": (average(HUMIDITY_SCD40) + average(HUMIDITY_SENSE_AIR)) / 2,
    })


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
