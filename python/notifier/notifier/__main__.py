import asyncio
import logging

from aiohttp import ClientSession
from dotenv import load_dotenv

from notifier.app import NotifierApp
from notifier.notifier_state_repo import NotifierStateRepository
from notifier.notifiers.co2_notifier import CO2Notifier

load_dotenv()

LOGGER = logging.getLogger(__name__)


async def _main():
    logging.basicConfig(
        format='%(asctime)s %(name)s %(levelname)-8s %(message)s',
        level=logging.DEBUG,
        datefmt='%Y-%m-%d %H:%M:%S'
    )
    client_session = ClientSession()
    notifier_app = NotifierApp(
        http_client=client_session,
        notifiers=[
            # ChaosMonkeyNotifier(),
            # RoutineNotifier(ROUTINE),
            CO2Notifier(client_session),
        ],
        notifier_state_repository=NotifierStateRepository('state.pkl'),
    )
    try:
        await notifier_app.run()
    except Exception as e:
        LOGGER.critical(e)
    finally:
        await client_session.close()


if __name__ == '__main__':
    asyncio.run(_main())
