from datetime import datetime
from typing import List

from notifier.entities import Notifier, Notification
from routines.routine_definition import Routine


class RoutineNotifier(Notifier):

    def __init__(self, routines: List[Routine]):
        self._routines = routines

    async def should_run(self, now: datetime) -> bool:
        for routine in self._routines:
            if routine.is_right_now(now):
                return True
        return False

    async def notify(self, now: datetime) -> List[Notification]:
        routines_should_run = [routine for routine in self._routines if routine.is_right_now(now)]
        for routine in routines_should_run:
            pass
            # print(routine)
