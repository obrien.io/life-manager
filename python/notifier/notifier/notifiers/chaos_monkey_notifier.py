import random
from datetime import datetime
from typing import List

from notifier.entities import Notifier, Notification


class ChaosMonkeyNotifier(Notifier):

    async def should_run(self, now: datetime) -> bool:
        roll = random.random()
        if roll > .5:
            raise RuntimeError("Chaos monkey failed to determine if it should be able to run")
        return True

    async def notify(self, now: datetime) -> List[Notification]:
        roll = random.random()
        if roll > .5:
            raise RuntimeError("Chaos monkey failed to create a notification")
        return [Notification("Chaos Monkey", "Success")]
