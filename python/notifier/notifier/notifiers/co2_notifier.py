from datetime import datetime
from typing import List, Optional

from aiohttp import ClientSession

from notifier.entities import Notifier, Notification


class CO2Notifier(Notifier):

    def __init__(
        self,
        http_client: ClientSession,
    ):
        self._client = http_client
        self._co2_level: Optional[float] = None

    async def should_run(self, now: datetime) -> bool:
        async with self._client.get('http://10.0.1.119:8000/stats') as response:
            response.raise_for_status()
            response_content = await response.json()
        self._co2_level = response_content["co2"]
        return self._co2_level > 1000

    async def notify(self, now: datetime) -> List[Notification]:
        return [Notification(
            "CO2 Level",
            f"CO2 level is too high ({self._co2_level}), open a window"
        )]
