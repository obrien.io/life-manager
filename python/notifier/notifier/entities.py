from dataclasses import dataclass
from datetime import datetime
from typing import List, Optional


@dataclass
class Notification:
    title: str
    message: str


class Notifier:

    async def should_run(self, now: datetime) -> bool:
        raise NotImplementedError

    async def notify(self, now: datetime) -> List[Notification]:
        raise NotImplementedError


class DoesntExist(Exception):
    pass


@dataclass
class NotifierState:
    name: str
    last_run_successful: bool
    last_successful_run: Optional[datetime]
    last_failure_notification: Optional[datetime]
    consecutive_failures: int

    @classmethod
    def create_default(cls, notifier_name: str):
        return NotifierState(
            notifier_name,
            last_run_successful=True,
            last_successful_run=None,
            last_failure_notification=None,
            consecutive_failures=0,
        )
