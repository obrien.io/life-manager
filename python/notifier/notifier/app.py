import logging
import os
from datetime import datetime, timedelta
from typing import List

from aiohttp import ClientSession
from humanfriendly import format_timespan

from notifier.entities import Notifier, Notification, NotifierState, DoesntExist
from notifier.notifier_state_repo import NotifierStateRepository

LOGGER = logging.getLogger(__name__)


class NotifierApp:

    def __init__(
        self,
        http_client: ClientSession,
        notifiers: List[Notifier],
        notifier_state_repository: NotifierStateRepository,
    ):
        self._http_client = http_client
        self._notifiers = notifiers
        self._notifier_state_repo = notifier_state_repository

    async def send_notification(self, notification: Notification):
        url = "https://api.pushover.net/1/messages.json"
        post_body = {
            "token": os.environ["PUSHOVER_APPLICATION_KEY"],
            "user": os.environ["PUSHOVER_USER_KEY"],
            "message": f"{notification.title}:\n{notification.message}",
        }
        async with self._http_client.post(
            url,
            data=post_body,
        ) as response:
            response.raise_for_status()

    async def _get_notifier_state(self, notifier_name: str):
        try:
            return await self._notifier_state_repo.get_notifier_state(notifier_name)
        except DoesntExist:
            return NotifierState.create_default(notifier_name)

    async def _notify_failed_notification(
        self,
        now: datetime,
        message: str,
        notifier_name: str,
        state: NotifierState,
        failure_notification_frequency: timedelta = timedelta(days=1),
    ):
        if not state.last_run_successful or state.last_successful_run is None:
            state.consecutive_failures += 1
        display_failures = state.consecutive_failures
        LOGGER.warning(
            f"{notifier_name} has failed to {message} - {display_failures} "
            f"{'times' if display_failures > 1 else 'time'}"
        )
        last_notification_delta = now - state.last_failure_notification
        if (
            state.last_failure_notification is None
            or last_notification_delta > failure_notification_frequency
        ):
            await self.send_notification(Notification(
                "Notification failure",
                f"Failed to {message} for {notifier_name}, "
                f"failure {state.consecutive_failures}",
            ))
            state.last_failure_notification = now
        else:
            LOGGER.debug(
                f"Not notifying for {notifier_name}, has only been "
                f"{format_timespan(last_notification_delta)} since the last notification")
        state.last_run_successful = False
        await self._notifier_state_repo.update_state(state)

    async def _mark_run_successful(self, now: datetime, state: NotifierState):
        # Update for successful run
        state.last_run_successful = True
        state.last_successful_run = now
        state.consecutive_failures = 0
        await self._notifier_state_repo.update_state(state)

    async def run(self):
        now = datetime.now()
        for notifier in self._notifiers:
            notifier_name = notifier.__class__.__name__
            state = await self._get_notifier_state(notifier_name)
            try:
                should_run = await notifier.should_run(now)
            except Exception as e:
                LOGGER.critical(e)
                await self._notify_failed_notification(
                    now,
                    "determine if notifier should run",
                    notifier_name,
                    state
                )
                continue
            if not should_run:
                continue
            try:
                notifications = await notifier.notify(now)
            except Exception as e:
                LOGGER.critical(e)
                await self._notify_failed_notification(
                    now,
                    "create notifications",
                    notifier_name,
                    state
                )
                continue
            for notification in notifications:
                await self.send_notification(notification)
            await self._mark_run_successful(now, state)
