import os
import pickle
from typing import Dict

import aiofiles as aiofiles

from notifier.entities import NotifierState, DoesntExist


class NotifierStateRepository:

    def __init__(self, cache_path: str):
        self._cache_path = cache_path

    async def _load(self) -> Dict[str, NotifierState]:
        if not os.path.exists(self._cache_path):
            return dict()
        async with aiofiles.open(self._cache_path, 'rb') as f:
            contents = await f.read()
        return pickle.loads(contents)

    async def _save(self, cache: Dict[str, NotifierState]):
        serialized = pickle.dumps(cache)
        async with aiofiles.open(self._cache_path, 'wb') as f:
            await f.write(serialized)

    async def get_notifier_state(self, notifier_name: str) -> NotifierState:
        cached = await self._load()
        if notifier_name not in cached:
            raise DoesntExist
        return cached[notifier_name]

    async def update_state(self, state: NotifierState):
        cached = await self._load()
        cached[state.name] = state
        await self._save(cached)
