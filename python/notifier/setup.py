from setuptools import find_packages, setup

setup(
    name="notifier",
    version="0.0.1",
    author="Andrew O'Brien",
    author_email="andrew@obrien.io",
    description="Notify me of things I need to be aware of",
    packages=find_packages(
        exclude=[
            'tests',
        ]
    ),
    install_requires=[
        "python-dotenv",
        "aiohttp",
        "humanfriendly",
    ],
    extras_require={},
)
