from setuptools import find_packages, setup

setup(
    name="routines",
    version="0.0.1",
    author="Andrew O'Brien",
    author_email="andrew@obrien.io",
    description="",
    packages=find_packages(),
    install_requires=[],
    extras_require={},
)
