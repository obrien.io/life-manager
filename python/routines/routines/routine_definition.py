from dataclasses import dataclass
from datetime import time, datetime
from enum import Enum
from typing import Optional


class Frequency(Enum):
    OneOff = 'one-off'  # a specific event
    Daily = 'daily'
    BiDaily = 'bi-daily'  # every two days
    SemiWeekly = 'semiweekly'  # twice a week
    Weekly = 'weekly'
    BiWeekly = 'biweekly'  # every two weeks
    Monthly = 'monthly'
    Quarterly = 'quarterly'
    Yearly = 'yearly'


class RoutineCategory(Enum):
    Grooming = 'grooming'
    Exercise = 'exercise'
    Chores = 'chores'
    Review = 'review'
    MorningRoutine = 'morning'
    EveningRoutine = 'evening'


class Event(Enum):
    # Sailing
    DepartSailing = 'leave-for-sailing'
    ReturnSailing = 'return-from-sailing'
    # Skiing
    DepartSkiing = 'leave-for-skiing'
    ReturnSkiing = 'return-from-skiing'
    # Weekend Trip
    DepartWeekend = 'leave-for-weekend'
    ReturnWeekend = 'return-from-weekend'
    # Week Trip
    DepartWeek = 'leave-for-week'
    ReturnWeek = 'return-from-week'


@dataclass
class Routine:
    name: str
    category: RoutineCategory
    frequency: Frequency
    time_of_day: Optional[time]
    day_of_week: Optional[int] = None
    last_completed_at: Optional[datetime] = None
    weekday_only: bool = False
    weekend_only: bool = False

    def is_right_now(self, now: datetime) -> bool:
        daily_routine_now = (
            self.frequency == Frequency.Daily
            # and self.time_of_day is not None
            # and self.time_of_day == now.time()
        )
        return daily_routine_now


_MORNING_ROUTINE_TIME = time(hour=6, minute=10)
_EVENING_ROUTINE_TIME = time(hour=9, minute=0)
ROUTINE = [
    # Grooming
    Routine("Cut hair", RoutineCategory.Grooming, Frequency.Monthly, None),
    Routine("Trim nails", RoutineCategory.Grooming, Frequency.BiWeekly, None),
    Routine("Exfoliate", RoutineCategory.Grooming, Frequency.Weekly, None),
    Routine("Wash face", RoutineCategory.Grooming, Frequency.BiDaily, None),
    Routine("Moisturize face", RoutineCategory.Grooming, Frequency.BiDaily, None),
    # Chores
    Routine("Vacuum apartment", RoutineCategory.Chores, Frequency.Weekly, None),
    Routine("Take out trash", RoutineCategory.Chores, Frequency.Weekly, None),
    Routine("Take out recycling", RoutineCategory.Chores, Frequency.Weekly, None),
    Routine("Clear dining room table", RoutineCategory.Chores, Frequency.BiDaily, None),
    Routine("Put tech away", RoutineCategory.Chores, Frequency.SemiWeekly, None),
    Routine("Laundry", RoutineCategory.Chores, Frequency.Weekly, None),
    # Specific rooms
    Routine("Clean kitchen", RoutineCategory.Chores, Frequency.BiWeekly, None),
    Routine("Clean bedroom", RoutineCategory.Chores, Frequency.BiWeekly, None),
    Routine("Clean living room", RoutineCategory.Chores, Frequency.BiWeekly, None),
    Routine("Clean office", RoutineCategory.Chores, Frequency.BiWeekly, None),
    Routine("Clean bathroom", RoutineCategory.Chores, Frequency.BiWeekly, None),
    # Review
    Routine("Workday review", RoutineCategory.Review, Frequency.Daily,
            time(hour=12 + 6, minute=0), weekday_only=True),
    Routine("Work week review", RoutineCategory.Review, Frequency.Weekly,
            time(hour=12 + 6, minute=0), day_of_week=5),
    Routine("Weekly review", RoutineCategory.Review, Frequency.Weekly, None),
    Routine("Monthly review", RoutineCategory.Review, Frequency.Monthly, None),
    Routine("Quarterly review", RoutineCategory.Review, Frequency.Quarterly, None),
    Routine("Yearly review", RoutineCategory.Review, Frequency.Yearly, None),
    # Exercise
    Routine("Strength training", RoutineCategory.Exercise, Frequency.BiDaily, None),
    Routine("Cardio training", RoutineCategory.Exercise, Frequency.BiDaily, None),

    # Morning routine
    Routine("Out of bed", RoutineCategory.MorningRoutine, Frequency.Daily, _MORNING_ROUTINE_TIME),
    Routine("Make bed", RoutineCategory.MorningRoutine, Frequency.Daily, _MORNING_ROUTINE_TIME),
    Routine("Weigh myself", RoutineCategory.MorningRoutine, Frequency.Daily, _MORNING_ROUTINE_TIME),
    Routine("Shower", RoutineCategory.MorningRoutine, Frequency.Daily, _MORNING_ROUTINE_TIME),
    Routine("Deodorant", RoutineCategory.MorningRoutine, Frequency.Daily, _MORNING_ROUTINE_TIME),
    Routine("Brush teeth", RoutineCategory.MorningRoutine, Frequency.Daily, _MORNING_ROUTINE_TIME),
    Routine("Shave", RoutineCategory.MorningRoutine, Frequency.BiDaily, _MORNING_ROUTINE_TIME),
    Routine("Get dressed", RoutineCategory.MorningRoutine, Frequency.Daily, _MORNING_ROUTINE_TIME),
    Routine("Brush hair", RoutineCategory.MorningRoutine, Frequency.Daily, _MORNING_ROUTINE_TIME),
    Routine("Take supplements", RoutineCategory.MorningRoutine, Frequency.Daily,
            _MORNING_ROUTINE_TIME),
    Routine("Morning coffee", RoutineCategory.MorningRoutine, Frequency.Daily,
            _MORNING_ROUTINE_TIME),
    # Evening routine
    Routine("Reset kitchen", RoutineCategory.EveningRoutine, Frequency.Daily,
            _EVENING_ROUTINE_TIME),
    Routine("Put away all laundry", RoutineCategory.EveningRoutine, Frequency.Daily,
            _EVENING_ROUTINE_TIME),
    Routine("Pick out tomorrow's outfit", RoutineCategory.EveningRoutine, Frequency.Daily,
            _EVENING_ROUTINE_TIME),
    Routine("Take sleep aid", RoutineCategory.EveningRoutine, Frequency.Daily,
            _EVENING_ROUTINE_TIME),
    Routine("Brush teeth", RoutineCategory.EveningRoutine, Frequency.Daily, _EVENING_ROUTINE_TIME),
    Routine("Phone in another room", RoutineCategory.EveningRoutine, Frequency.Daily,
            _EVENING_ROUTINE_TIME),
    Routine("Alarms set", RoutineCategory.EveningRoutine, Frequency.Daily, _EVENING_ROUTINE_TIME),
]
