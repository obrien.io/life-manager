import asyncio
import logging
from dataclasses import dataclass
from datetime import date
from typing import List

from routines.routine_definition import Routine, Frequency, ROUTINE

LOGGER = logging.getLogger(__name__)


@dataclass
class Day:
    day: date


@dataclass
class Schedule:
    pass


def _filter_routine_items_by_freq(items: List[Routine], freq: Frequency) -> List[Routine]:
    return list(filter(lambda item: item.frequency == freq, items))


async def schedule_routine(
    items: List[Routine],
    start: date,
    end: date
):
    # semi_weekly = _filter_routine_items_by_freq(items, Frequency.SemiWeekly)
    # weekly = _filter_routine_items_by_freq(items, Frequency.Weekly)
    # print(semi_weekly)
    daily = _filter_routine_items_by_freq(items, Frequency.Daily)
    timely_events = []
    for task in daily:
        if task.time_of_day:
            timely_events.append(task)
    pass


async def main():
    schedule = await schedule_routine(ROUTINE)


if __name__ == '__main__':
    asyncio.run(main())
